//
//  CCDataParser.m
//  CurrencyConversion
//
//  Created by Nikita Belopotapov on 13/07/2017.
//  Copyright © 2017 Nikita Belopotapov. All rights reserved.
//

#import "CCDataParser.h"
#import <UIKit/UIKit.h>


@interface CCDataParser() <NSXMLParserDelegate>

@property (nonatomic, strong) NSXMLParser *xmlParser;
@property (nonatomic, copy) NSDictionary *parseDictionary;

@end


@implementation CCDataParser

- (instancetype)initWithData:(NSData *)data {
    self = [super init];
    
    if (self) {
        _xmlParser = [[NSXMLParser alloc] initWithData:data];
        _xmlParser.delegate = self;
        
        _parseDictionary = [NSDictionary dictionaryWithObject:@(1) forKey:@"EUR"];
    }
    
    return self;
}

- (NSDictionary *)parse {
    [self.xmlParser parse];
    return self.parseDictionary;
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    NSString *currnecyCode = attributeDict[@"currency"];
    
    if (![currnecyCode isEqualToString:@"USD"] && ![currnecyCode isEqualToString:@"GBP"]) {
        return;
    }

    CGFloat rate = [attributeDict[@"rate"] floatValue];
    if (rate) {
        NSMutableDictionary *parseDictionaryCopy = [self.parseDictionary mutableCopy];
        [parseDictionaryCopy setObject:@(rate) forKey:currnecyCode];
        self.parseDictionary = [parseDictionaryCopy copy];
    }
}

@end
