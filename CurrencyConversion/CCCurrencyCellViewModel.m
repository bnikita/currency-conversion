//
//  CCCurrencyCellViewModel.m
//  CurrencyConversion
//
//  Created by Nikita Belopotapov on 17/07/2017.
//  Copyright © 2017 Nikita Belopotapov. All rights reserved.
//

#import "CCCurrencyCellViewModel.h"
#import "CCCurrency.h"
#import "CCCurrencyRate.h"


@interface CCCurrencyCellViewModel()

@property (nonatomic, strong) CCCurrency *currentCurrency;
@property (nonatomic, strong) CCCurrencyRate *currentCurrencyRate;
@property (nonatomic, strong) CCCurrencyRate *exchangeCurrencyRate;
@property (nonatomic, assign, getter=isOutcomeCell) BOOL outcomeCell;
@property (nonatomic, assign) CGFloat exchangeSum;

@end


@implementation CCCurrencyCellViewModel

+ (instancetype)cellViewModelWith:(CCCurrency *)currency
                     currencyRate:(CCCurrencyRate *)currencyRate
                    isOutcomeCell:(BOOL)outcomeCell {
    CCCurrencyCellViewModel *cellViewModel = [CCCurrencyCellViewModel new];
    cellViewModel.currentCurrency = currency;
    cellViewModel.currentCurrencyRate = currencyRate;
    cellViewModel.outcomeCell = outcomeCell;
    
    return cellViewModel;
}

- (NSString *)titleForUsersCurrencyLabel {
    CGFloat usersCurrencyValue = self.currentCurrency.value ?: 0;
    NSString *usersCurrencyText = [NSString stringWithFormat:@"You have %@%.02f", self.currentCurrency.sign, usersCurrencyValue];
    return usersCurrencyText;
}

- (UIColor *)textColorForUsersCurrencyLabelWithInputValue:(NSString *)inputValue {
    if (!self.isOutcomeCell) {
        return [UIColor lightTextColor];
    }
    
    UIColor *textColor;
    if (ABS(inputValue.integerValue) > self.currentCurrency.value) {
        textColor = [UIColor redColor];
    } else {
        textColor = [UIColor lightTextColor];
    }
    
    return textColor;
}

- (NSString *)exchangeRateString {
    if (!self.exchangeCurrencyRate || !self.currentCurrencyRate) {
        return @"";
    }
    
    if (!self.isOutcomeCell) {
        return [NSString stringWithFormat:@"%@%@ = %.02f%@", self.currentCurrencyRate.sign, @"1", self.exchangeCurrencyRate.rate / self.currentCurrencyRate.rate, self.exchangeCurrencyRate.sign];
    } else{
        return @"";
    }
}

- (NSString *)newInputValueForInput:(CGFloat)inputValue; {
    if (!inputValue) {
        return @"";
    }
    NSString *additionalString = self.isOutcomeCell ? @"-" : @"+";
    if (!self.exchangeCurrencyRate || !self.currentCurrencyRate) {
        return [NSString stringWithFormat:@"%@%.0f", additionalString, inputValue];
    }
    
    CGFloat exchangeRate = self.isOutcomeCell ? self.currentCurrencyRate.rate / self.exchangeCurrencyRate.rate : self.exchangeCurrencyRate.rate / self.currentCurrencyRate.rate;
    CGFloat exchangeValue = self.isOutcomeCell ? inputValue * exchangeRate : inputValue / exchangeRate;
    NSString *resultString = [NSString stringWithFormat:@"%@%.02f", additionalString, exchangeValue];
    return resultString;
}




@end
