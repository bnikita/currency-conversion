//
//  CCCurrency.m
//  CurrencyConversion
//
//  Created by Nikita Belopotapov on 13/07/2017.
//  Copyright © 2017 Nikita Belopotapov. All rights reserved.
//

#import "CCCurrency.h"

@interface CCCurrency()

@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) CGFloat value;
@property (nonatomic, copy) NSString *sign;

@end

@implementation CCCurrency

- (instancetype)initWithName:(NSString *)name valueString:(NSString *)valueString sign:(NSString *)sign {
    self = [super init];
    if (self) {
        _name = name;
        _value = valueString.floatValue;
        _sign = sign;
    }
    
    return self;
}

- (void)updateValue:(CGFloat)newValue {
    self.value = newValue;
}

@end
