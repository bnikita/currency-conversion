//
//  CCDataParser.h
//  CurrencyConversion
//
//  Created by Nikita Belopotapov on 13/07/2017.
//  Copyright © 2017 Nikita Belopotapov. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CCDataParser : NSObject

- (instancetype)init NS_UNAVAILABLE;

/**
 Initialization method

 @param data NSData to parse
 @return self
 */
- (instancetype)initWithData:(NSData *)data;

/**
 Method starts parsing process

 @return parsed data
 */
- (NSDictionary *)parse;

@end
