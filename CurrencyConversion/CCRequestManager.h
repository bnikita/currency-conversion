//
//  CCRequestManager.h
//  CurrencyConversion
//
//  Created by Nikita Belopotapov on 13/07/2017.
//  Copyright © 2017 Nikita Belopotapov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"


typedef void (^ResponseHandler)(id response, NSError *error);


@interface CCRequestManager : NSObject

- (instancetype)init NS_UNAVAILABLE;

/**
 Initialization

 @return self
 */
+ (instancetype)sharedManager;

/**
 Method sends get request

 @param requestString request URI
 @param parameters request parameters
 @param handler completion handler
 */
- (void)sendGetRequest:(NSString *)requestString parameters:(NSDictionary *)parameters withHandler:(ResponseHandler)handler;

@end
