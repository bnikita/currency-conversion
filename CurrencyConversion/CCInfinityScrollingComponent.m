//
//  CCInfinityScrollingComponent.m
//  CurrencyConversion
//
//  Created by Nikita Belopotapov on 22/07/2017.
//  Copyright © 2017 Nikita Belopotapov. All rights reserved.
//

#import "CCInfinityScrollingComponent.h"

@interface CCInfinityScrollingComponent() <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, weak) id<CCInfinityScrollingComponentDelegate>delegate;
@property (nonatomic, weak) UICollectionView *collectionView;
@property (nonatomic, copy) NSArray *originalDataObjects;
@property (nonatomic, copy) NSArray *fakeDataObjects;
@property (nonatomic, assign) CGFloat currenctCollectionViewContentOffset;

@end

@implementation CCInfinityScrollingComponent

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView
                              delegate:(id<CCInfinityScrollingComponentDelegate>)delegate
                    collectionViewData:(NSArray *)dataObjects {
    self = [super init];
    if (self) {
        _delegate = delegate;
        _collectionView = collectionView;
        _originalDataObjects = dataObjects;
        [self prepareDataObjects];
        [self prepareCollectionView];
    }
    return self;
}

- (void)prepareDataObjects {
    NSMutableArray *originalDataObjectsCopy = [_originalDataObjects mutableCopy];
    [originalDataObjectsCopy addObject:_originalDataObjects.firstObject];
    if (originalDataObjectsCopy.count > 2) {
        [originalDataObjectsCopy insertObject:_originalDataObjects.lastObject atIndex:0];
    }
    _fakeDataObjects = [originalDataObjectsCopy copy];
}

- (void)prepareCollectionView {
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:1 inSection:0];
    [_collectionView scrollToItemAtIndexPath:indexPath
                            atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                    animated:NO];
}

// ТУТ?
- (NSInteger)originalIndexForItemAtIndex:(NSInteger)index {
    NSInteger difference = index - 1;
    if (difference < 0) {
        return ABS((self.originalDataObjects.count + difference) % self.originalDataObjects.count);
    } else if (difference < self.originalDataObjects.count) {
        return difference;
    } else {
        return ABS((difference - self.originalDataObjects.count) % self.originalDataObjects.count);
    }
    
    return 0;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return collectionView.bounds.size;
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.fakeDataObjects.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(cellForItemAtIndexPath:realIndex:collectionView:)]) {
        NSInteger originalIndex = [self originalIndexForItemAtIndex:indexPath.row];
        return [self.delegate cellForItemAtIndexPath:indexPath realIndex:originalIndex collectionView:self.collectionView];
    }
    return nil;
}


#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat boundarySize = CGRectGetWidth(self.collectionView.bounds);
    CGFloat contentOffsetValue = scrollView.contentOffset.x;
    if (contentOffsetValue >= self.collectionView.contentSize.width - boundarySize) {
        CGPoint newOffsetPoint = CGPointMake(boundarySize, 0);
        scrollView.contentOffset = newOffsetPoint;
    } else if (contentOffsetValue <= 0) {
        CGFloat lessBoundarySize = self.originalDataObjects.count * boundarySize;
        CGPoint newOffsetPoint = CGPointMake(lessBoundarySize, 0);
        scrollView.contentOffset = newOffsetPoint;
    }
    
    self.currenctCollectionViewContentOffset = scrollView.contentOffset.x;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([self.delegate respondsToSelector:@selector(collectionViewDidChangeCell:)]) {
        [self.delegate collectionViewDidChangeCell:self.collectionView];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(collectionView:didEndDisplayingCell:)]) {
        [self.delegate collectionView:collectionView didEndDisplayingCell:cell];
    }
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(collectionView:willDisplayCell:)]) {
        [self.delegate collectionView:collectionView willDisplayCell:cell];
    }
}

@end
