//
//  ViewController.m
//  CurrencyConversion
//
//  Created by Nikita Belopotapov on 13/07/2017.
//  Copyright © 2017 Nikita Belopotapov. All rights reserved.
//

#import "ViewController.h"
#import "CCCurrencyViewModel.h"
#import "CCInfinityScrollingComponent.h"
#import <ReactiveObjC/ReactiveObjC.h>
#import "CCExchangeCollectionViewCell.h"
#import "CCCurrencyCellViewModel.h"


NSString *const ViewControllerOutcomeCellReuseIdentifier = @"outcomeCell";
NSString *const ViewControllerIncomeCellReuseIdentifier = @"incomeCell";


@interface ViewController () <CCInfinityScrollingComponentDelegate, UITextFieldDelegate, CCExchangeCollectionViewCellDelegate>

@property (nonatomic, strong) CCCurrencyViewModel *viewModel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomCollectionViewBottomConstraint;

@property (weak, nonatomic) IBOutlet UICollectionView *outcomeCollectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *outcomeCollectionViewPageControl;
@property (nonatomic, strong) CCInfinityScrollingComponent *outcomeInfinityScroll;

@property (weak, nonatomic) IBOutlet UICollectionView *incomeCollectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *incomeCollectionViewPageControl;
@property (nonatomic, strong) CCInfinityScrollingComponent *incomeInfinityScroll;

@property (weak, nonatomic) IBOutlet UIButton *exchangeButton;

@end

@implementation ViewController


#pragma mark - Lyfecicle

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillAppear:)
                                                 name:UIKeyboardWillChangeFrameNotification
                                               object:nil];
    
    self.viewModel = [CCCurrencyViewModel new];
    [self updateExchangeButton:NO];
    
    [self setupCollectionViews];
    [self startObserving];
}

- (void)viewDidLayoutSubviews {
    if (!self.outcomeInfinityScroll && self.viewModel.currencyRateArray) {
        [self setupInfinityScroll];
    }
}

- (void)setupCollectionViews {
    [self.outcomeCollectionView registerNib:[UINib nibWithNibName:@"CCExchangeCollectionViewCell" bundle:nil]
                 forCellWithReuseIdentifier:ViewControllerOutcomeCellReuseIdentifier];
    
    [self.incomeCollectionView registerNib:[UINib nibWithNibName:@"CCExchangeCollectionViewCell" bundle:nil]
                forCellWithReuseIdentifier:ViewControllerIncomeCellReuseIdentifier];
}

- (void)startObserving {
    @weakify(self);
    [RACObserve(self.viewModel, currencyRateArray) subscribeNext:^(NSArray *currencyRates) {
        @strongify(self);
        if (currencyRates.count) {
            if (!self.outcomeInfinityScroll) {
                [self.view setNeedsLayout];
            }
            [self updateCollectionViews];
        }
    }];
    
    [[RACObserve(self.viewModel, error) ignore:nil] subscribeNext:^(NSError *error) {
        @strongify(self);
        [self presentViewController:[self alertWithError:error] animated:YES completion:nil];
    }];
}

- (void)setupInfinityScroll {
    self.outcomeInfinityScroll = [[CCInfinityScrollingComponent alloc] initWithCollectionView:self.outcomeCollectionView
                                                                                     delegate:self
                                                                           collectionViewData:self.viewModel.currencyRateArray];
    
    self.incomeInfinityScroll = [[CCInfinityScrollingComponent alloc] initWithCollectionView:self.incomeCollectionView
                                                                                    delegate:self
                                                                          collectionViewData:self.viewModel.currencyRateArray];
}

- (void)updateCollectionViews {
    [self.incomeCollectionView performBatchUpdates:^{
        [self.incomeCollectionView reloadItemsAtIndexPaths:[self.incomeCollectionView indexPathsForVisibleItems]];
    } completion:^(BOOL finished) {
        [self collectionViewDidChangeCell:self.incomeCollectionView];
    }];
    [self.outcomeCollectionView performBatchUpdates:^{
        [self.outcomeCollectionView reloadItemsAtIndexPaths:[self.outcomeCollectionView indexPathsForVisibleItems]];
    } completion:^(BOOL finished) {
        [self collectionViewDidChangeCell:self.outcomeCollectionView];
    }];
}


#pragma mark - Custom Actions

- (void)keyboardWillAppear:(NSNotification *)notification {
    CGRect keyboardFrame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardHeight = CGRectGetHeight(keyboardFrame);
    [UIView animateWithDuration:0.3 animations:^{
        self.bottomCollectionViewBottomConstraint.constant = keyboardHeight;
        
        [self.incomeCollectionView.collectionViewLayout invalidateLayout];
        [self.outcomeCollectionView.collectionViewLayout invalidateLayout];
        
        [self.outcomeCollectionView layoutIfNeeded];
        [self.incomeCollectionView layoutIfNeeded];
    }];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    NSLog(@"KEYBOARD!!!!!");
}

- (IBAction)exchangeButtonTapped:(UIButton *)sender {
    CCExchangeCollectionViewCell *incomeCell = self.incomeCollectionView.visibleCells.firstObject;
    CCExchangeCollectionViewCell *outcomeCell = self.outcomeCollectionView.visibleCells.firstObject;
    
    [self.viewModel convertCurrenciesOutcome:outcomeCell income:incomeCell];
}

- (void)updateExchangeButton:(BOOL)canExchange {
    CGFloat alpha = canExchange ? 1.0 : 0.5;
    self.exchangeButton.alpha = alpha;
}


#pragma mark - CCInfinityScrollingComponentDelegate

- (UICollectionViewCell *)cellForItemAtIndexPath:(NSIndexPath *)indexPath realIndex:(NSInteger)realIndex collectionView:(UICollectionView *)collectionView {
    BOOL isOutcome = collectionView == self.outcomeCollectionView;
    NSString *reuseIdentifier = isOutcome ? ViewControllerOutcomeCellReuseIdentifier : ViewControllerIncomeCellReuseIdentifier;
    
    CCExchangeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier
                                                                                 forIndexPath:indexPath];
    
    [self.viewModel setupCell:cell atIndex:realIndex delegate:self isOutcome:isOutcome];
    
    if (isOutcome && !self.isEditing && indexPath.row == 1) {
        [cell startEditing];
    }
    
    return cell;
}

- (void)collectionViewDidChangeCell:(UICollectionView *)collectionView {
    NSInteger index = [collectionView indexPathsForVisibleItems].firstObject.row - 1;
    
    CCExchangeCollectionViewCell *incomeCell = self.incomeCollectionView.visibleCells.firstObject;
    CCExchangeCollectionViewCell *outcomeCell = self.outcomeCollectionView.visibleCells.firstObject;
    BOOL canExchange = incomeCell.cellViewModel.currentCurrencyRate != outcomeCell.cellViewModel.currentCurrencyRate;
    [self updateExchangeButton:canExchange];
    
    CCCurrencyRate *incomeRate = incomeCell.cellViewModel.currentCurrencyRate;
    CCCurrencyRate *outcomeRate = outcomeCell.cellViewModel.currentCurrencyRate;
    if (incomeRate != outcomeRate) {
        [outcomeCell.cellViewModel setExchangeCurrencyRate:incomeRate];
        [incomeCell.cellViewModel setExchangeCurrencyRate:outcomeRate];
    } else {
        [outcomeCell.cellViewModel setExchangeCurrencyRate:nil];
        [incomeCell.cellViewModel setExchangeCurrencyRate:nil];
    }

    [incomeCell updateInputValue:[outcomeCell inputValue]];
    if (collectionView == self.outcomeCollectionView) {
        self.outcomeCollectionViewPageControl.currentPage = index;
    } else {
        self.incomeCollectionViewPageControl.currentPage = index;
    }
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell {
    if ([cell.reuseIdentifier isEqualToString:ViewControllerOutcomeCellReuseIdentifier]) {
        [(CCExchangeCollectionViewCell *)cell startEditing];
        [self.viewModel setOldInputValueForCell:(CCExchangeCollectionViewCell *)cell];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell {
    if ([cell.reuseIdentifier isEqualToString:ViewControllerOutcomeCellReuseIdentifier]) {
        [self.viewModel saveInputForCell:(CCExchangeCollectionViewCell *)cell];
    }
}


#pragma mark - CCExchangeCollectionViewCellDelegate

- (void)inputValue:(CGFloat)value changedForCell:(CCExchangeCollectionViewCell *)cell; {
    CCExchangeCollectionViewCell *anotherCell;
    if  ([self.incomeCollectionView.visibleCells containsObject:cell]) {
        anotherCell = self.outcomeCollectionView.visibleCells.firstObject;
        
    } else {
        anotherCell = self.incomeCollectionView.visibleCells.firstObject;
    }
    [anotherCell updateInputValue:value];
}

- (UIAlertController *)alertWithError:(NSError *)error {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error"
                                                                             message:error.userInfo[NSLocalizedDescriptionKey]
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *closeAction = [UIAlertAction actionWithTitle:@"OK"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    
    [alertController addAction:closeAction];
    return alertController;
}

@end
