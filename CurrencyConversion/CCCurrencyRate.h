//
//  CCCurrencyRate.h
//  CurrencyConversion
//
//  Created by Nikita Belopotapov on 17/07/2017.
//  Copyright © 2017 Nikita Belopotapov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCCurrencyInitializationProtocol.h"

@interface CCCurrencyRate : NSObject <CCCurrencyInitializationProtocol>

@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) CGFloat rate; // Currency rate value base on EUR
@property (nonatomic, readonly) NSString *sign;

@end
