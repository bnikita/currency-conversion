//
//  CCCurrencyInitializationProtocol.h
//  CurrencyConversion
//
//  Created by Nikita Belopotapov on 17/07/2017.
//  Copyright © 2017 Nikita Belopotapov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CCCurrencyInitializationProtocol <NSObject>

/**
 Currency Objects Initialization

 @param name Currency Name
 @param valueString Currency Value - Rate or Amount
 @param sign ASCII Sign
 @return self
 */
- (instancetype)initWithName:(NSString *)name valueString:(NSString *)valueString sign:(NSString *)sign;

- (instancetype)init NS_UNAVAILABLE;

@end
