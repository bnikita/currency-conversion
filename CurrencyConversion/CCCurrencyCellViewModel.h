//
//  CCCurrencyCellViewModel.h
//  CurrencyConversion
//
//  Created by Nikita Belopotapov on 17/07/2017.
//  Copyright © 2017 Nikita Belopotapov. All rights reserved.
//

#import <UIKit/UIKit.h>


@class CCCurrency;
@class CCCurrencyRate;


@interface CCCurrencyCellViewModel : NSObject

@property (nonatomic, readonly) CCCurrency *currentCurrency;
@property (nonatomic, readonly) CCCurrencyRate *currentCurrencyRate;
@property (nonatomic, readonly) CCCurrencyRate *exchangeCurrencyRate;
@property (nonatomic, readonly, getter=isOutcomeCell) BOOL outcomeCell;

@property (nonatomic, readonly) CGFloat exchangeSum;

/**
 Init Method

 @param currency User's currency
 @param currencyRate Currency rate for user's currency
 @param outcomeCell tells whether cell is outcome
 @return self
 */
+ (instancetype)cellViewModelWith:(CCCurrency *)currency currencyRate:(CCCurrencyRate *)currencyRate isOutcomeCell:(BOOL)outcomeCell;

/**
 Return title for currency label

 @return NSString
 */
- (NSString *)titleForUsersCurrencyLabel;

/**
 Returns color for user's currency text label

 @param inputValue input value - will be compared with existing currency value
 @return UIColor
 */
- (UIColor *)textColorForUsersCurrencyLabelWithInputValue:(NSString *)inputValue;

/**
 Returns rate for 2 currentcy rates

 @return NSString
 */
- (NSString *)exchangeRateString;

/**
 Calculate new value depending on value from another cell

 @param inputValue input value from another cell
 @return NSString
 */
- (NSString *)newInputValueForInput:(CGFloat)inputValue;

// setters for readonly properties
- (void)setExchangeCurrencyRate:(CCCurrencyRate *)exchangeCurrencyRate;
- (void)setExchangeSum:(CGFloat)exchangeSum;

@end
