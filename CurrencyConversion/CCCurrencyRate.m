//
//  CCCurrencyRate.m
//  CurrencyConversion
//
//  Created by Nikita Belopotapov on 17/07/2017.
//  Copyright © 2017 Nikita Belopotapov. All rights reserved.
//

#import "CCCurrencyRate.h"

@interface CCCurrencyRate()

@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) CGFloat rate;
@property (nonatomic, copy) NSString *sign;

@end

@implementation CCCurrencyRate

- (instancetype)initWithName:(NSString *)name valueString:(NSString *)valueString sign:(NSString *)sign {
    self = [super init];
    if (self) {
        _name = name;
        _rate = valueString.floatValue;
        _sign = sign;
    }
    
    return self;
}

@end
