//
//  CCIncomeCollectionViewCell.m
//  CurrencyConversion
//
//  Created by Nikita Belopotapov on 23/07/2017.
//  Copyright © 2017 Nikita Belopotapov. All rights reserved.
//

#import "CCExchangeCollectionViewCell.h"
#import "CCCurrencyCellViewModel.h"
#import "CCCurrency.h"
#import "CCCurrencyRate.h"
#import <ReactiveObjC/ReactiveObjC.h>


@interface CCExchangeCollectionViewCell() <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *currencyNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *exchangeSumTextField;
@property (weak, nonatomic) IBOutlet UILabel *exchangeRateLabel;
@property (weak, nonatomic) IBOutlet UILabel *usersCurrencyLabel;

@property (nonatomic, strong) RACDisposable *racExchangeCurrencyRateObserve;

@end


@implementation CCExchangeCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.exchangeSumTextField.delegate = self;
    [self.exchangeSumTextField addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingChanged];
}

- (void)textChanged:(UITextField *)textField {
    self.usersCurrencyLabel.textColor = [self.cellViewModel textColorForUsersCurrencyLabelWithInputValue:textField.text];
    if ([self.delegate respondsToSelector:@selector(inputValue:changedForCell:)]) {
        [self.delegate inputValue:ABS(textField.text.floatValue) changedForCell:self];
    }
}

- (void)setCellViewModel:(CCCurrencyCellViewModel *)cellViewModel {
    _cellViewModel = cellViewModel;
    
    self.currencyNameLabel.text = cellViewModel.currentCurrencyRate.name;
    self.usersCurrencyLabel.text = [cellViewModel titleForUsersCurrencyLabel];
    if (cellViewModel.isOutcomeCell && cellViewModel.exchangeSum) {
        self.exchangeSumTextField.text = [NSString stringWithFormat:@"-%.0f", self.cellViewModel.exchangeSum];
        self.usersCurrencyLabel.textColor = [cellViewModel textColorForUsersCurrencyLabelWithInputValue:self.exchangeSumTextField.text];
    }
    
    @weakify(self);
    [self.racExchangeCurrencyRateObserve dispose];
    self.racExchangeCurrencyRateObserve = [[RACObserve(cellViewModel, exchangeCurrencyRate)
                                            takeUntil:[self rac_prepareForReuseSignal]]
                                           subscribeNext:^(CCCurrencyRate *exchangeCurrencyRate) {
                                               @strongify(self);
                                               self.exchangeRateLabel.text = [self.cellViewModel exchangeRateString];
                                           }];
}

- (void)updateInputValue:(CGFloat)inputValue {
    self.exchangeSumTextField.text = [self.cellViewModel newInputValueForInput:inputValue];
    self.usersCurrencyLabel.textColor = [self.cellViewModel textColorForUsersCurrencyLabelWithInputValue:self.exchangeSumTextField.text];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (string.length && range.location == 0) {
        NSString *firstCharacter = self.cellViewModel.isOutcomeCell ? @"-" : @"+";
        textField.text = [NSString stringWithFormat:@"%@%@", firstCharacter, textField.text];
    } else if (!string.length && range.location == 1) {
        textField.text = @"";
        if ([self.delegate respondsToSelector:@selector(inputValue:changedForCell:)]) {
            [self.delegate inputValue:0 changedForCell:self];
        }
    }
    
    return YES;
}

- (void)startEditing {
    if (![self isEditing]) {
        [self.exchangeSumTextField becomeFirstResponder];
    }
}

- (BOOL)isEditing {
    return self.exchangeSumTextField.isEditing;
}

- (CGFloat)inputValue {
    return ABS(self.exchangeSumTextField.text.floatValue);
}

@end
