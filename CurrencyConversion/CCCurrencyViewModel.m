//
//  CCCurrencyViewModel.m
//  CurrencyConversion
//
//  Created by Nikita Belopotapov on 13/07/2017.
//  Copyright © 2017 Nikita Belopotapov. All rights reserved.
//

#import "CCCurrencyViewModel.h"
#import <UIKit/UIKit.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import "CCCurrencyDataProvider.h"
#import "CCCurrency.h"
#import "CCCurrencyRate.h"
#import "CCCurrencyCellViewModel.h"
#import "CCExchangeCollectionViewCell.h"


@interface CCCurrencyViewModel() 

@property (nonatomic, strong) CCCurrencyDataProvider *currencyDataProvider;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, copy) NSArray <CCCurrencyRate *> *currencyRateArray;
@property (nonatomic, copy) NSArray <CCCurrency *> *fakeUsersCurrencies;
@property (nonatomic, assign) BOOL exchangeButtonEnabled;
@property (nonatomic, copy) NSDictionary <NSString *, NSNumber *> *inputValues;
@property (nonatomic, strong) NSError *error;

@end


@implementation CCCurrencyViewModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _currencyDataProvider = [CCCurrencyDataProvider new];
        
        _timer = [NSTimer scheduledTimerWithTimeInterval:30
                                                  target:self
                                                selector:@selector(getData)
                                                userInfo:nil
                                                 repeats:YES];
        [_timer fire];
        
        _fakeUsersCurrencies = @[
                                 [[CCCurrency alloc] initWithName:@"USD" valueString:@"100" sign:@"$"],
                                 [[CCCurrency alloc] initWithName:@"EUR" valueString:@"100" sign:@"€"],
                                 [[CCCurrency alloc] initWithName:@"GBP" valueString:@"100" sign:@"£"],
                                 ];
        
        _inputValues = [NSDictionary dictionary];
    }
    
    return self;
}

- (void)getData {
    @weakify(self);
    [self.currencyDataProvider getCurrencyData:^(NSArray *currencyPairs, NSError *error) {
        @strongify(self);
        self.currencyRateArray = currencyPairs;
        self.error = error;
    }];
}

- (void)convertCurrenciesOutcome:(CCExchangeCollectionViewCell *)outcomeCell
                          income:(CCExchangeCollectionViewCell *)incomeCell {
    CCCurrency *outcomeCurrency = outcomeCell.cellViewModel.currentCurrency;
    CCCurrency *incomeCurrency = incomeCell.cellViewModel.currentCurrency;
    if (outcomeCurrency == incomeCurrency) {
        return;
    }

    CGFloat exchangeSum = [outcomeCell inputValue];
    if (outcomeCurrency.value < exchangeSum) {
        return;
    }

    [outcomeCurrency updateValue:outcomeCurrency.value - exchangeSum];

    CCCurrencyRate *incomeRate = incomeCell.cellViewModel.exchangeCurrencyRate;
    CCCurrencyRate *outcomeRate = outcomeCell.cellViewModel.exchangeCurrencyRate;

    CGFloat exchangeRate = outcomeRate.rate / incomeRate.rate;

    [incomeCurrency updateValue:incomeCurrency.value + exchangeSum * exchangeRate];
    
    CCCurrencyCellViewModel *outcomeCellViewModel = [CCCurrencyCellViewModel cellViewModelWith:outcomeCurrency
                                                                           currencyRate:incomeRate
                                                                          isOutcomeCell:YES];
    
    outcomeCell.cellViewModel = outcomeCellViewModel;
    
    CCCurrencyCellViewModel *incomeCellViewModel = [CCCurrencyCellViewModel cellViewModelWith:incomeCurrency
                                                                                  currencyRate:outcomeRate
                                                                                 isOutcomeCell:NO];
    
    incomeCell.cellViewModel = incomeCellViewModel;
}

- (void)setupCell:(CCExchangeCollectionViewCell *)cell
          atIndex:(NSInteger)index
         delegate:(id<CCExchangeCollectionViewCellDelegate>)delegate
        isOutcome:(BOOL)isOutcome {
    
    cell.delegate = delegate;
    
    CCCurrencyRate *currencyRate = self.currencyRateArray[index];
    CCCurrency *currentUsersCurrency = [self userCurrencyForRate:currencyRate];

    CCCurrencyCellViewModel *cellViewModel = [CCCurrencyCellViewModel cellViewModelWith:currentUsersCurrency
                                                                           currencyRate:currencyRate
                                                                          isOutcomeCell:isOutcome];
    
    [cellViewModel setExchangeSum:self.inputValues[currentUsersCurrency.name].floatValue];
    cell.cellViewModel = cellViewModel;
}

- (void)saveInputForCell:(CCExchangeCollectionViewCell *)cell {
    NSMutableDictionary *inputValuesCopy = [self.inputValues mutableCopy];
    inputValuesCopy[cell.cellViewModel.currentCurrency.name] = @([cell inputValue]);
    self.inputValues = [inputValuesCopy copy];
}

- (void)setOldInputValueForCell:(CCExchangeCollectionViewCell *)cell {
    NSNumber *inputValue = self.inputValues[cell.cellViewModel.currentCurrency.name];
    if (inputValue) {
        [cell.cellViewModel setExchangeSum:inputValue.floatValue];
    }
}


#pragma mark - Private 

- (CCCurrency *)userCurrencyForRate:(CCCurrencyRate *)currencyRate {
    NSPredicate *currencyPredicate = [NSPredicate predicateWithFormat:@"self.sign == %@", currencyRate.sign];
    CCCurrency *currentUsersCurrency = [self.fakeUsersCurrencies filteredArrayUsingPredicate:currencyPredicate].firstObject;
    return currentUsersCurrency;
}

@end
