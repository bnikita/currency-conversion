//
//  CCRequestManager.m
//  CurrencyConversion
//
//  Created by Nikita Belopotapov on 13/07/2017.
//  Copyright © 2017 Nikita Belopotapov. All rights reserved.
//

#import "CCRequestManager.h"
#import "AFNetworking.h"
#import "CCDataParser.h"


NSString *const CCRequestManagerBaseURL = @"http://www.ecb.europa.eu/";


@interface CCRequestManager()

@property (nonatomic, strong) AFHTTPSessionManager *sessionManager;

@end


@implementation CCRequestManager

+ (instancetype)sharedManager {
    static CCRequestManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [CCRequestManager new];
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfiguration.requestCachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
        sessionConfiguration.URLCache = nil;
        manager.sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:CCRequestManagerBaseURL] sessionConfiguration:sessionConfiguration];
        manager.sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.sessionManager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.sessionManager.requestSerializer.timeoutInterval = 10;
    });
    return manager;
}


#pragma mark - Requests

- (void)sendGetRequest:(NSString *)request parameters:(NSDictionary *)parameters withHandler:(ResponseHandler)handler {
    [self.sessionManager GET:request
                  parameters:parameters
                    progress:nil
                     success:^(NSURLSessionDataTask *task, id responseObject) {
                         if (handler) {
                             [self handleResponse:responseObject responseHandler:handler];
                         }
                     }
                     failure:^(NSURLSessionDataTask *task, NSError *error) {
                         if (handler) {
                             handler(nil, error);
                         }
                     }];
}


#pragma mark - Response handlers

- (void)handleResponse:(id)responseObject responseHandler:(ResponseHandler)handler {
    if (![responseObject isKindOfClass:[NSData class]]) {
        
        NSString *errorString = @"Error";
        NSString *errorDescriptionString = @"Unknown server error";
        
        NSError *error = [NSError errorWithDomain:errorString
                                             code:0
                                         userInfo:@{NSLocalizedDescriptionKey: errorDescriptionString}];
        handler(nil, error);
    }
    
    handler(responseObject, nil);
}


@end
