//
//  CCCurrency.h
//  CurrencyConversion
//
//  Created by Nikita Belopotapov on 13/07/2017.
//  Copyright © 2017 Nikita Belopotapov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCCurrencyInitializationProtocol.h"

@interface CCCurrency : NSObject <CCCurrencyInitializationProtocol>

@property (nonatomic, readonly) NSString *name; 
@property (nonatomic, readonly) CGFloat value; // Users currency value
@property (nonatomic, readonly) NSString *sign;

/**
 Method updates value of users currency after convertation

 @param newValue <#newValue description#>
 */
- (void)updateValue:(CGFloat)newValue;

@end
