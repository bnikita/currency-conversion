//
//  CCInfinityScrollingComponent.h
//  CurrencyConversion
//
//  Created by Nikita Belopotapov on 22/07/2017.
//  Copyright © 2017 Nikita Belopotapov. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol CCInfinityScrollingComponentDelegate <NSObject>

/**
 Analogue of standart cellForItemAtIndexPath

 @param indexPath cell's index path in collection view
 @param realIndex real index of cell
 @param collectionView collection view that displays cell
 @return configured UICollectionViewCell
 */
- (UICollectionViewCell *)cellForItemAtIndexPath:(NSIndexPath *)indexPath realIndex:(NSInteger)realIndex collectionView:(UICollectionView *)collectionView;

/**
 Method tells that collectionView changed visible cell

 @param collectionView UICollectionView on focus
 */
- (void)collectionViewDidChangeCell:(UICollectionView *)collectionView;

@optional
// Methods are analogues of UICollectionViewDelegate methods

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell;
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell;

@end


@interface CCInfinityScrollingComponent : NSObject

- (instancetype)init NS_UNAVAILABLE;

/**
 Initialization method - should always be used;

 @param collectionView collection view to apply infinity scroll
 @param delegate delegate object that will setup cells at react on scroll events
 @param dataObjects NSArray of data
 @return self
 */
- (instancetype)initWithCollectionView:(UICollectionView *)collectionView
                              delegate:(id<CCInfinityScrollingComponentDelegate>)delegate
                    collectionViewData:(NSArray *)dataObjects;


@end

NS_ASSUME_NONNULL_END
