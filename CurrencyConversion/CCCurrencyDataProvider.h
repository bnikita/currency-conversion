//
//  CCCurrencyDataProvider.h
//  CurrencyConversion
//
//  Created by Nikita Belopotapov on 13/07/2017.
//  Copyright © 2017 Nikita Belopotapov. All rights reserved.
//

#import "CCRequestManager.h"


@interface CCCurrencyDataProvider : NSObject

/**
 Method send request to currency service to get currency rates

 @param response completion block with array of data of error
 */
- (void)getCurrencyData:(void (^)(NSArray *currencyPairs, NSError *error))response;

@end
