//
//  CCIncomeCollectionViewCell.h
//  CurrencyConversion
//
//  Created by Nikita Belopotapov on 23/07/2017.
//  Copyright © 2017 Nikita Belopotapov. All rights reserved.
//

#import <UIKit/UIKit.h>


@class CCCurrencyCellViewModel;
@class CCExchangeCollectionViewCell;


@protocol CCExchangeCollectionViewCellDelegate <NSObject>

/**
 Method tells delegate that cell changed input

 @param value New value for input
 @param cell CCExchangeCollectionViewCell
 */
- (void)inputValue:(CGFloat)value changedForCell:(CCExchangeCollectionViewCell *)cell;

@end


@interface CCExchangeCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) CCCurrencyCellViewModel *cellViewModel;
@property (nonatomic, weak) id<CCExchangeCollectionViewCellDelegate>delegate;

- (void)updateInputValue:(CGFloat)inputValue;
- (CGFloat)inputValue;
- (void)startEditing;
- (BOOL)isEditing;

@end
