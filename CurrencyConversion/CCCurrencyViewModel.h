//
//  CCCurrencyViewModel.h
//  CurrencyConversion
//
//  Created by Nikita Belopotapov on 13/07/2017.
//  Copyright © 2017 Nikita Belopotapov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CCExchangeCollectionViewCell.h"


@class CCCurrency;
@class CCCurrencyRate;
@class CCExchangeCollectionViewCell;


@interface CCCurrencyViewModel : NSObject

@property (nonatomic, readonly) NSArray <CCCurrencyRate *> *currencyRateArray;
@property (nonatomic, readonly) NSError *error;

/**
 Method setups cell

 @param cell Cell to be setup
 @param index Index of object for cell
 @param delegate delegate object to respond cell's actions
 @param isOutcome whether cell is outcome
 */
- (void)setupCell:(CCExchangeCollectionViewCell *)cell
          atIndex:(NSInteger)index
         delegate:(id<CCExchangeCollectionViewCellDelegate>)delegate
        isOutcome:(BOOL)isOutcome;

/**
 Converts 2 currencies

 @param outcomeCell cell with data about outcome currency
 @param incomeCell cell with data about income currency
 */
- (void)convertCurrenciesOutcome:(CCExchangeCollectionViewCell *)outcomeCell
                          income:(CCExchangeCollectionViewCell *)incomeCell;

/**
 Saves input value for scrolled out cell

 @param cell CCExchangeCollectionViewCell
 */
- (void)saveInputForCell:(CCExchangeCollectionViewCell *)cell;

/**
 returns saved value to cell's text field

 @param cell CCExchangeCollectionViewCell
 */
- (void)setOldInputValueForCell:(CCExchangeCollectionViewCell *)cell;

@end
