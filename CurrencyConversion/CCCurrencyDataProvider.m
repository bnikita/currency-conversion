//
//  CCCurrencyDataProvider.m
//  CurrencyConversion
//
//  Created by Nikita Belopotapov on 13/07/2017.
//  Copyright © 2017 Nikita Belopotapov. All rights reserved.
//

#import "CCCurrencyDataProvider.h"
#import "CCRequestManager.h"
#import "CCDataParser.h"
#import "CCCurrencyRate.h"

@interface CCCurrencyDataProvider()

@property (nonatomic, strong) CCRequestManager *requestManager;

@end

@implementation CCCurrencyDataProvider

- (instancetype)init {
    self = [super init];
    if (self) {
        _requestManager = [CCRequestManager sharedManager];
    }
    
    return self;
}

- (void)getCurrencyData:(void (^)(NSArray *currencyPairs, NSError *error))response {
    [self.requestManager sendGetRequest:@"stats/eurofxref/eurofxref-daily.xml"
                             parameters:nil
                            withHandler:^(NSData *responseData, NSError *error) {
                                if (!error) {
                                    CCDataParser *dataParser = [[CCDataParser alloc] initWithData:responseData];
                                    NSDictionary *responseDictionary = [dataParser parse];
                                    response([self parseResponseToCurrencyPairs:responseDictionary], nil);
                                } else {
                                    response(nil, error);
                                }
                            }];
}

- (NSArray <CCCurrencyRate *> *)parseResponseToCurrencyPairs:(NSDictionary *)response {
    NSMutableArray *currencyPairs = [NSMutableArray array];
    for (NSString *name in response.allKeys) {
        NSString *rate = response.allValues[[response.allKeys indexOfObject:name]];
        CCCurrencyRate *currencyRate = [[CCCurrencyRate alloc] initWithName:name valueString:rate sign:[self signForCurrencyName:name]];
        [currencyPairs addObject:currencyRate];
    }
    return [currencyPairs copy];
}

- (NSString *)signForCurrencyName:(NSString *)name {
    static NSDictionary *signsDictionary = nil;
    static dispatch_once_t dipatch_token;
    dispatch_once(&dipatch_token, ^{
        signsDictionary = @{
                            @"USD": @"$",
                            @"EUR": @"€",
                            @"GBP": @"£",
                            };
    });
    
    return signsDictionary[name];
    
}





@end
